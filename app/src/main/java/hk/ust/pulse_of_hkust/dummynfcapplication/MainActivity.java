package hk.ust.pulse_of_hkust.dummynfcapplication;


import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.nio.charset.Charset;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private NfcAdapter nfcAdaptor;
    private PendingIntent nfcPendingIntent;
    private IntentFilter[] ndefExchangeFilters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT <
                Build.VERSION_CODES.JELLY_BEAN_MR1) {
            // If Android Beam isn't available, don't continue.
            /*
             * Disable Android Beam file transfer features here.
             */
            Toast.makeText(this, "The preferred NFC functionality does not support on this phone.", Toast.LENGTH_LONG).show();

            // Android Beam file transfer is available, continue
        } else {
            Toast.makeText(this, "NFC supports on this phone.", Toast.LENGTH_LONG).show();
            nfcAdaptor = NfcAdapter.getDefaultAdapter(this);
        }

        nfcPendingIntent = PendingIntent.getActivity(
                this,
                0,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),
                0
        );

        IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);

        try {
            ndefDetected.addDataType("text/plain");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            Log.e(TAG, "cannot add Data Type text/plain to ndefDeteced. Error: " + e.getMessage());
        }

        ndefExchangeFilters = new IntentFilter[]{ndefDetected};

    }

    private void enableNdefExchangeMode() {

        nfcAdaptor.enableForegroundNdefPush(
                MainActivity.this,
                new NdefMessage(new NdefRecord[]{
                        createTextRecord("this is the testing string", true)
                })
        );
        nfcAdaptor.enableForegroundDispatch(this, nfcPendingIntent, ndefExchangeFilters, null);
    }

    @Override
    public void onResume() {
        super.onResume();
        enableNdefExchangeMode();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // NDEF exchange mode
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            NdefMessage[] msgs = getNdefMessages(intent);
            getMessageFromOtherDevice(msgs[0]);
            Toast.makeText(this, "sent the dummy text via nfc!", Toast.LENGTH_LONG).show();
        }
    }

    private void getMessageFromOtherDevice(NdefMessage msg) {
        String receiveMessage = new String(msg.getRecords()[0].getPayload());
        Toast.makeText(this, "Get the MdefMessage[0] = " + receiveMessage, Toast.LENGTH_LONG).show();
    }

    private NdefMessage[] getNdefMessages(Intent intent) {
        // Parse the intent
        NdefMessage[] msgs = null;
        String action = intent.getAction();

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];

                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }

            } else {
                // Unknown tag type
                byte[] empty = new byte[]{};
                NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, empty, empty);
                NdefMessage msg = new NdefMessage(new NdefRecord[]{
                        record
                });

                msgs = new NdefMessage[]{msg};
            }
        } else {
            Log.d(TAG, "Unknown intent.");
            finish();
        }
        return msgs;
    }

    private NdefRecord createTextRecord(String payload, boolean encodeInUtf8) {

        Charset utfEncoding = encodeInUtf8 ? Charset.forName("UTF-8") : Charset.forName("UTF-16");
        byte[] textBytes = payload.getBytes(utfEncoding);

        int utfBit = encodeInUtf8 ? 0 : (1 << 7);
        byte[] data = new byte[1 + textBytes.length];
        System.arraycopy(textBytes, 0, data, 1, textBytes.length);

        NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
                NdefRecord.RTD_TEXT, new byte[0], data);

        return record;
    }
}
